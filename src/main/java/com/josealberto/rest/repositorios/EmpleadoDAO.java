package com.josealberto.rest.repositorios;

import com.josealberto.rest.empleados.Capacitacion;
import com.josealberto.rest.empleados.Empleado;
import com.josealberto.rest.empleados.Empleados;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Repository
public class EmpleadoDAO {
    Logger logger = LoggerFactory.getLogger(EmpleadoDAO.class);
    private static Empleados list = new Empleados();
    static {
        Capacitacion  cap1 = new Capacitacion("2020/01/01", "DBA");
        Capacitacion  cap2 = new Capacitacion("2020/01/01", "BackEnd");
        Capacitacion  cap3 = new Capacitacion("2018/01/01", "Front End");
        ArrayList<Capacitacion> una = new ArrayList<Capacitacion>();
        una.add(cap1);
        ArrayList<Capacitacion> dos = new ArrayList<Capacitacion>();
        dos.add(cap1);
        dos.add(cap2);

        ArrayList<Capacitacion> todos = new ArrayList<Capacitacion>();
        todos.add(cap3);
        todos.addAll(dos);


        list.getListaEmpleados().add(new Empleado(1,"Jose Alberto","Garcia","josealberto.garcia@bbva.com", una));
        list.getListaEmpleados().add(new Empleado(2,"Angel Matias","Garcia","angelmatias.garcia@bbva.com", dos));
        list.getListaEmpleados().add(new Empleado(3,"Maria de los Angeles","Sanchez","mariaangeles.sanchez@bbva.com",todos));
    }

    public Empleados getAllEmpleados(){
        logger.debug("Mostrar todos los empleados");
        return list;
    }

    public Empleado getEmpleado(int id){
        return list.getListaEmpleados().stream().filter(empleado -> empleado.getId() ==id).findAny().orElse(null);
    }

    public void addEmpleado(Empleado empleado){
        list.getListaEmpleados().add(empleado);
    }

    public void updateEmpleado(Empleado empleado){
        Empleado current = getEmpleado(empleado.getId());
        current.setNombre(empleado.getNombre());
        current.setApellido(empleado.getApellido());
        current.setEmail(empleado.getEmail());
    }

    public void  updateEmpleado(int id, Empleado empleado){
        Empleado current =getEmpleado(id);
        current.setNombre(empleado.getNombre());
        current.setApellido(empleado.getApellido());
        current.setEmail(empleado.getEmail());
    }
    /*
    public String deleteEmpleado(int id){
        Empleado current = getEmpleado(id);
        if(current==null) return "No";
        Iterator it = list.getListaEmpleados().iterator();
        while (it.hasNext()){
            Empleado emp= (Empleado) it.next();
            if(emp.getId() == id){
                it.remove();
                break;
            }
        }
        return "Ok";
    }

     */

    public String deleteEmpleado(int id){
       if(list.getListaEmpleados().removeIf(empleado -> empleado.getId()==id)){
           return "Ok";
        }else{
           return "No";
       }
    }

    public void sofUpdate(int id, Map<String, Object> updates){
        Empleado current = getEmpleado(id);
        for(Map.Entry<String,Object> update : updates.entrySet()){
            switch (update.getKey()){

                case "nombre" :
                    current.setNombre(update.getValue().toString());
                    break;
                case "apellido" :

                    current.setApellido(update.getValue().toString());
                    break;
                case "emil" :
                    current.setEmail(update.getValue().toString());
            }

        }

    }

    public List<Capacitacion> getCapacitacionesEmpleado(int id){
        Empleado empleado = getEmpleado(id);
        ArrayList<Capacitacion> caps = new ArrayList<Capacitacion>();
        if(empleado!=null){
            caps =empleado.getCapacitaciones();
        }
        return caps;
    }

    public Boolean addCapacitacion(int id, Capacitacion capacitacion){
        Empleado empleado = getEmpleado(id);
        if(empleado==null){
            return false;
        }else{
            empleado.getCapacitaciones().add(capacitacion);
            return true;
        }
    }
}
