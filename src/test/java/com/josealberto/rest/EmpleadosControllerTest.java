package com.josealberto.rest;

import com.josealberto.rest.utils.BadSeparator;
import com.josealberto.rest.utils.EstadosPedido;
import com.josealberto.rest.utils.Utilidades;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import org.junit.jupiter.params.provider.NullAndEmptySource;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.test.context.SpringBootTest;
import static org.assertj.core.api.Fail.fail;
import static org.junit.jupiter.api.Assertions.*;

@EnableConfigurationProperties
@SpringBootTest
public class EmpleadosControllerTest {

    @Autowired
    EmpleadosController empleadosController;

    @Test
    void testGetAutor(){
        assertEquals("\"Jose Garcia\"", empleadosController.getAutorByConfigClass());
    }

    @Test
    void testGetCadena() throws BadSeparator {
        String expectedValue="J-O-S-E A-L-B-E-R-T-O";
        String value = empleadosController.getCadena("jose alberto","-");
        assertEquals(expectedValue, value);
    }

    @Test
    void testSeparadorGetCadena() throws BadSeparator {
        try {
            Utilidades.getCadena("Jose Alberto", "..");
            fail("Se esperaba BadSeparator");
        }catch(BadSeparator bs){

        }
    }

    @ParameterizedTest
    @ValueSource(ints = {1,3,5,17,-3, Integer.MAX_VALUE})
    void testEsImpar(int numero){
        assertTrue(Utilidades.esImpar(numero));
    }

    @ParameterizedTest
    @ValueSource(strings = {"", " "})
    void testEstaBlanco(String cadena){
        assertTrue(Utilidades.isBlank(cadena));
    }

    @ParameterizedTest
    @NullAndEmptySource
    @ValueSource(strings = {"", " ","\t","\n"})
    void testEstaBlancoCompleto(String cadena){
        assertTrue(Utilidades.isBlank(cadena));
    }

    @ParameterizedTest
    @EnumSource(EstadosPedido.class)
    void testValorarEstadoPedido(EstadosPedido ep){
        assertTrue(Utilidades.valorarEstadoPedido(ep));
    }



}
